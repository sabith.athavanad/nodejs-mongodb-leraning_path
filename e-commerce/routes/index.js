var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {

  let products = [{
    name: "Samsung M21",
    category: "Mobile",
    description: "One of the samsung's Best Phone",
    image: "images/m21.jpg"
  },
  {
    name: "Samsung S10",
    category: "Mobile",
    description: "One of the samsung's Best Phone",
    image: "images/s10.jpg"
  }, {
    name: "Samsung S20",
    category: "Mobile",
    description: "One of the samsung's Best Phone",
    image: "images/s20.jpg"
  }, {
    name: "Samsung A30s",
    category: "Mobile",
    description: "One of the samsung's Best Phone",
    image: "images/a30s.jpg"
  },
  ]
  res.render('index', { products, admin: false });
});

module.exports = router;
