var express = require('express');
var router = express.Router();
var productHelper = require('../helpers/product-helpers')

/* GET users listing. */
router.get('/', function (req, res, next) {
  productHelper.getAllProducts().then((products)=>{

    res.render('admin/view-products.hbs', { admin: true, products });
  })
  
});

router.get('/add-product', (req, res) => {
  res.render('admin/add-product')
})

router.post('/add-product', (req, res) => {

  productHelper.addProduct(req.body, (id) => {
    let Image = req.files.image
    console.log(id)
    Image.mv('./public/product-img/' + id + '.jpg', (err, done) => {
      if (err)
        console.log("error is : " + err)

      else
        console.log("success")
        res.render('admin/add-product')

    })

  })
})
module.exports = router;
