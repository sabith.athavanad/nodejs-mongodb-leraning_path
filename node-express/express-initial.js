const express=require('express')
const path=require('path')
var bodyParser = require('body-parser')

var app=express()


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
  })); 

app.use(function(req,res,next){
    console.log('use function loaded')
    next()
})

app.get('/signup',function(req,res){
     res.sendFile(path.join(__dirname,'form.html'))
})

app.post('/signup',function(req,res){
    
    var name = req.body.firstName;
    console.log(req.body)
    res.send("Form loaded")
})

app.get('/about',(req,res)=> res.send('about page'))



app.listen(3000,()=> console.log("server started"))