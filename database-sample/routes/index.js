var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/submit',(req,res)=>{
  MongoClient.connect('mongodb://localhost:27017',(err,client)=>{
    if(err)
      console.log('error in db connection')
    else
      client.db('test_db').collection('user').insertOne(req.body)

  })
  console.log(req.body)
  res.send("got the parameters")
})

module.exports = router;
