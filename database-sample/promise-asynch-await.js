const { resolve, reject } = require('promise')
const Promise = require('promise')

function getName(){
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
            resolve("Sabith")
        },3000)
    })
}
async function getFunction(){
    let name = await getName()
    console.log(name)
}

getFunction()
